from django.shortcuts import render,redirect
from .models import Info
def list(request):
    object=Info.objects.all()
    context={'object':object}
    template_name="myapp/list.html"
    return render(request,template_name,context)

def create(request):
    if request.method=="POST":
        info = Info(
            name=request.POST['name'],
            address=request.POST['address'],
            phone_number=request.POST['phone']
        )
        info.save()
        return redirect('list')
    template_name = "myapp/create.html"
    return render(request, template_name)

def update(request,id):
    if request.method=="POST":
        info=Info.objects.get(id=id)
        info.name=request.POST['name']
        info.address=request.POST['address']
        info.phone_number=request.POST['phone']
        info.save()
        return redirect('list')
    info = Info.objects.get(id=id)
    context={'info':info}
    template_name = "myapp/update.html"
    return render(request, template_name,context)
def delete(request,id):
    info=Info.objects.get(id=id)
    info.delete()