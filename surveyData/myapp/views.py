from django.shortcuts import render
from django.shortcuts import HttpResponse
import MySQLdb,json

def index(request):
	return render(request,'myapp/index.html')

def json_data(request):
	db=MySQLdb.connect(host="localhost",user="root",passwd="nirusha123",db="backend_challenge")
	cur=db.cursor()
	sql=("SELECT SurveyID,District,Municipality,Age,Gender FROM SurveyData")
	cur.execute(sql)
	data=cur.fetchall()
	columns = [desc[0] for desc in cur.description]
	result = []
	for row in data:
		row = dict(zip(columns, row))
		result.append(row)
	data1=json.dumps(result)
	return HttpResponse(data1,content_type="application/json")
	return render(request,'myapp/json_data.html')