from django.conf.urls import url
from django.contrib import admin
from myapp import views
urlpatterns = [
	url(r'^index$', views.index, name="index"),
    url(r'^json_data$', views.json_data, name="json_data"),
]